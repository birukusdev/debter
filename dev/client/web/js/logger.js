

function hidelogger(){
  $("#logwindow").css("z-index",1);
  $("#logwindowmenu").css("z-index",1);
  $("#logwindow_alllogs").css("z-index",1);
  $("#logwindow_latestlogs").css("z-index",1);
  $("#logwindow").visibility="hidden";
}
function showlogger(){
  $("#logwindow").css("z-index",10000);
  $("#logwindowmenu").css("z-index",10000);
  $("#logwindow_alllogs").css("z-index",10000);
  $("#logwindow_latestlogs").css("z-index",10001);
  $("#logwindow").visibility="visible";
}
function showalllog(){
  $("#logwindow_alllogs").css("z-index",10001);
  $("#logwindow_latestlogs").css("z-index",10000);
  $("#current_showing_log_window").html("Showing All Logs");
}
function showlatestlog(){
  $("#logwindow_alllogs").css("z-index",10000);
  $("#logwindow_latestlogs").css("z-index",10001);
  $("#current_showing_log_window").html("Showing Latest Logs");
}
function clearlatestlog(){
  $("#logwindow_latestlogs").html("");
}

function taglogmsg(title,msg,color,bgcolor){
  var t = new Date();
  tstr = (t.getMonth()+1)+"/"+t.getDate()+"/"+t.getFullYear()+"-"+t.getHours()+":"+t.getMinutes()+":"+t.getSeconds();
  var tag = "<p "+' style="background-color:'+bgcolor+'; color:'+color+'; "> <b>'+title+'</b> : <span style="font-size:10px;">'+tstr+"</span><br>"+msg+"</p>";
  return tag;
}

function addlog(log){
  $("#logwindow_alllogs").html( $("#logwindow_alllogs").html()+log);
  $("#logwindow_latestlogs").html( log+$("#logwindow_latestlogs").html());
}
function warn(msg){
  addlog(taglogmsg("WARNING",msg,"#87420a","#f9e8d9"));
}
function note(msg){
  addlog(taglogmsg("NOTE",msg,"#18217c","#dcdeef"));
}
function error(msg){
  addlog(taglogmsg("ERROR",msg,"#590a0a","#e5d5d5"));
}
