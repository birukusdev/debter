var header_div_height = 30;
var footer_div_height = 30;
var menu1_div_height = 50;
var menu2_div_height = 50;

//var nodebrowser_div_width = 230;

var content_divider_div_left = 230;
var content_divider_div_width = 7;

var nodebrowser_menu_div_height = 100;

var content_div_top = 0;
var content_div_height = 100;

var nodeviewer_title_div_height = 50;
var nodeviewer_menu_div_height = 50;


var content_divider_click_start_left =0;
var content_divider_click_end_left =0;

function content_divider_move_start(){
  content_divider_click_start_left = cursorX;
  $("#content_divider_div").css("background-color","#003300");
  //alert("left="+content_divider_click_start_left);
}
function content_divider_move_end(){
  $("#content_divider_div").css("background-color","#00ff00");
  content_divider_click_end_left = cursorX;
  diff = content_divider_click_end_left + content_divider_click_start_left;
  content_divider_div_left = content_divider_div_left + diff;
  page_resize();
}

function page_resize(){
    var screen_width = $(window).width();
    var screen_height = $(window).height();

    // set header_div
    $("#header_div").height(header_div_height);
    $("#header_div").width(screen_width);
    // set footer_div
    $("#footer_div").height(footer_div_height);
    $("#footer_div").width(screen_width);
    header_div_outer_height = $("#header_div").outerHeight();
    footer_div_outer_height = $("#footer_div").outerHeight();
    footer_top = screen_height-footer_div_outer_height;
    $("#footer_div").offset({top:footer_top,left:0});
    main_top_pxl = header_div_outer_height;
    main_bottom_pxl = screen_height-footer_div_outer_height;

    // set main_div
    main_height = main_bottom_pxl - main_top_pxl;
    $("#main_div").height(main_height);
    $("#main_div").width(screen_width);
    $("#main_div").offset({top:header_div_outer_height,left:0});

    // menu_div
    menu1_top = header_div_outer_height;
    $("#menu1_div").width(screen_width);
    $("#menu1_div").height(menu1_div_height);
    $("#menu1_div").offset({top:menu1_top,left:0});
    menu1_outer_height = $("#menu1_div").outerHeight();
    menu2_top = header_div_outer_height+menu1_outer_height;
    $("#menu2_div").width(screen_width);
    $("#menu2_div").height(menu2_div_height);
    $("#menu2_div").offset({top:menu2_top,left:0});
    $("#menu2_div").html("menu1height="+menu1_top+" menu2top="+menu2_top);
    menu2_outer_height = $("#menu2_div").outerHeight();
    // content_div
    content_div_height = screen_height - (header_div_height + footer_div_height + menu1_outer_height + menu2_outer_height);
    content_div_top = (menu2_top + menu2_outer_height);
    $("#content_div").width(screen_width);
    $("#content_div").height(content_div_height);
    $("#content_div").offset({top:content_div_top,left:0});

    // pages
    nodebrowser_div_width = content_divider_div_left;
    $("#nodebrowser_div").height(content_div_height);
    $("#nodebrowser_div").width(nodebrowser_div_width);
    $("#nodebrowser_div").offset({top:content_div_top,left:0});

    $("#nodebrowser_menu_div").height(nodebrowser_menu_div_height);
    $("#nodebrowser_menu_div").width(nodebrowser_div_width);
    $("#nodebrowser_menu_div").offset({top:content_div_top,left:0});

    $("#nodebrowser_main_div").height(content_div_height-nodebrowser_menu_div_height);
    $("#nodebrowser_main_div").width(nodebrowser_div_width);
    $("#nodebrowser_main_div").offset({top:content_div_top+nodebrowser_menu_div_height,left:0});

    nodebrowser_outer_width = $("#nodebrowser_div").outerWidth();
    content_divider_left = nodebrowser_outer_width;
    $("#content_divider_div").width(content_divider_div_width);
    $("#content_divider_div").height(content_div_height);
    $("#content_divider_div").offset({top:content_div_top,left:content_divider_div_left});
    content_divider_outer_width = $("#content_divider_div").outerWidth();
    nodeviewer_div_left = nodebrowser_outer_width + content_divider_outer_width;
    nodeviewer_div_width =  $("#content_div").width()-nodebrowser_outer_width-content_divider_outer_width;
    $("#nodeviewer_div").width(nodeviewer_div_width);
    $("#nodeviewer_div").height(content_div_height);
    $("#nodeviewer_div").offset({top:content_div_top,left:nodeviewer_div_left});
    nodeviewer_div_top = content_div_top;

    // pages div
    $("#nodeviewer_title_div").width(nodeviewer_div_width);
    $("#nodeviewer_title_div").height(nodeviewer_title_div_height);
    $("#nodeviewer_title_div").offset({top:nodeviewer_div_top,left:nodeviewer_div_left});

    $("#nodeviewer_menu_div").width(nodeviewer_div_width);
    $("#nodeviewer_menu_div").height(nodeviewer_menu_div_height);
    $("#nodeviewer_menu_div").offset({top:nodeviewer_div_top+nodeviewer_title_div_height,left:nodeviewer_div_left});

    nodeviewer_data_top = nodeviewer_div_top+nodeviewer_title_div_height+nodeviewer_menu_div_height;
    nodeviewer_data_height = content_div_height - (nodeviewer_title_div_height+nodeviewer_menu_div_height);

    $("#nodeviewer_html_div").width(nodeviewer_div_width);
    $("#nodeviewer_html_div").height(nodeviewer_data_height);
    $("#nodeviewer_html_div").offset({top:nodeviewer_data_top,left:nodeviewer_div_left});

    $("#nodeviewer_text_div").width(nodeviewer_div_width);
    $("#nodeviewer_text_div").height(nodeviewer_data_height);
    $("#nodeviewer_text_div").offset({top:nodeviewer_data_top,left:nodeviewer_div_left});
}
