
function getPosition() {
    if (window.getSelection) {
        sel = window.getSelection();
        if (sel.getRangeAt) {
            return sel.getRangeAt(0).startOffset;
        }
    }
    return null;
}

function insertHtmlAtCursor(html) {
    var range, node;

    if (window.getSelection && window.getSelection().getRangeAt) {
        range = window.getSelection().getRangeAt(0);
        node = range.createContextualFragment(html);
        range.insertNode(node);
        alert("option1:"+range);
    } else if (document.selection && document.selection.createRange) {
        document.selection.createRange().pasteHTML(html);
        alert("option2:");
    }

}

function surroundObject(elem){
  var selObj = window.getSelection();

  var selRange = selObj.getRangeAt(0);
  selRange.surroundContents(elem);
  alert(selObj+" "+selRange);
}

function formatHighlightedText(tag) {
    var sel, range;
    if (window.getSelection) {
        sel = window.getSelection();
        replacementText = String("<"+tag+">"+String(sel)+"</"+tag+">");
        if (sel.rangeCount) {
            range = sel.getRangeAt(0);
            range.deleteContents();
            range.insertNode(document.createTextNode(replacementText));
        }
    } else if (document.selection && document.selection.createRange) {
        range = document.selection.createRange();
        range.text = replacementText;
    }
}

function addDrawIOImage(){
    insertHtmlAtCursor(newImage());
}

function changeEqn(spanID,idname){
  var element = document.getElementById(spanID);  // You could use class
  var currentEqn = element.getAttribute('equation');  //123456789

  eqn = String(prompt("Equation",currentEqn));
  element.setAttribute('equation',eqn);
  var eqnhtml = katex.renderToString(eqn);
  //alert("span:"+spanID+"   = "+ $("'#"+spanID+"'").attr("title"));
  //katex.render(eqn, spanID);
  element.innerHTML = eqnhtml;
  //$("#testmath").html(katex.render("c = \\pm\\sqrt{a^2 + b^2}", "testmath"));
  //$("#"+idname).html(eqn);
}

function addEqn(){
  randtit = String("eqn_"+String(Math.random()*1000+Math.random()*1000));
  title = String(prompt("EquationTitle",randtit));
  eqn = '<span id="'+title+'" style="background-color:#eeeeee;" equation="f(a,b,c) = (a^2+b^2+c^2)^3" \
  ondblclick="changeEqn(this.id,\''+title+'\');">[[ EQN ]]</span>'
  insertHtmlAtCursor(eqn);

  //changeEqn(title);
}

function makeBold(){
  var page = document.getElementById("pages_content")
  alert(page.selectionStart + ","+ page.selectionEnd);
  //formatHighlightedText("b");
}
