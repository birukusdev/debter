#!/usr/bin/python
'''

    Gojo python version

'''

__author__ = 'btesfaye'

import os, sys, time
sys.path.append(os.path.join(os.path.dirname(__file__)+"/../"))

import argparse

from src.controller import Controller

VERBOSE=False


import signal
import sys

def signal_handler(signal, frame):
        print('GOJO: System shutdown requested. Shuttind down program')

        Controller().close()
        sys.exit(0)

signal.signal(signal.SIGINT, signal_handler)
signal.signal(signal.SIGTERM, signal_handler)



if __name__=="__main__":

    print("-----------------------")
    print(" _____     _     _       ")
    print("|     |   / \\   | |        ")
    print("| | | |___| |__ | |_ ___ _ __ ")
    print("| | | / _ \\ '_ \\| __/ _ \\ '__|")
    print("| |/ /  __/ |_) | ||  __/ |   ")
    print("|___/ \\___|_.__/ \\__\\___|_|   ")
    print("")
    print("-----------------------")
    print(" -- -- -- -- -- -- -- --")

    # parse input arguments
    parser = argparse.ArgumentParser()
    parser.add_argument("-p","--port", type=int, default=8008, help="[optional] port number for websocket [default 8008]")
    parser.add_argument("-v", "--verbose", action="store_true",help="describe every step")
    parser.add_argument("-c", "--config", type=str, default="./config.json", help=" path to config file")

    # mutually exclusive (chose one or the other)
    group = parser.add_mutually_exclusive_group()
    group.add_argument("-g", "--gui", action="store_true",help="Display QT GUI")
    group.add_argument("-w", "--web", action="store_true",help="Use websockets ")

    args = parser.parse_args()

    # select verbose
    VERBOSE=args.verbose

    print("Status: Controller Started")
    control = Controller(args.config, args.gui,args.web,args.port,args.verbose) # constructs a new object
    #control.init #setup UI
    print("--: Program starting")
    Controller().run() #start

    time.sleep(2)
    print("--: Program running")

    print('Press Ctrl+C to exit')
    signal.pause()


