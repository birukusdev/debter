'''
    This handles all the events and makes things happen

'''

from src.util.singleton import Singleton

from src.config.params import Params

from src.config.logger import Logger

from src.util.MutexLock import MutexLock

from src.debter.pagemanager import PageManager


# This class is a singleton
class EventsProcessor(MutexLock,metaclass=Singleton):

    def __init__(self):
        '''
            Initialize the class
        :return:
        '''

        # call the mutex lock
        super(EventsProcessor,self).__init__()

        # Params() Logger()
        self.pages = PageManager()

    '''
        Requests
            request_type:  "pagerequest" , "filerequest"

            request:
                pagerequest:
                    getpagelist
                    createdir
                    deletedir
                    getpagedata
                    createpage
                    deletepage

            pagerequest_shortpath:

    '''


    def processGETEvent(self,message):
        '''

        :param message:
        :return:
        '''

        # {system, time, devicetype, deviceid, sensorlabel, event }

        self.threadLock()

        msg_header = "text/html"
        msg = "<h1>testing</h1>".encode()

        Logger().writeNote("EventsProcessor: Get request made:")

        # pass page requests
        if "request_type" in message and message["request_type"][0] == "pagerequest":
            msg_header = "application/json"
            msg = self.__processPageEvent(message)


        self.threadUnlock()

        return msg,msg_header

    def processPOSTEvent(self, message):
        '''

        :param message:
        :return:
        '''

        self.threadLock()

        msg_header = "text/html"
        msg = "<h1>testing</h1>".encode()

        Logger().writeNote("EventsProcessor: Get request made:")

        # pass page requests
        if "request_type" in message and message["request_type"] == "pagerequest":
            msg_header = "application/json"
            msg = self.__processPageEvent(message)

        self.threadUnlock()

        return msg, msg_header


    def __processPageEvent(self,message):
        '''
            Possible requests are

            - get page list

            - get page data

            - write page data

            - create dir

            -

        '''

        if "request" not in message or "pagerequest_shortpath" not in message:
            return '[ "status":"error processing request"] '.encode()

        request = message["request"][0]
        request_shortpath = message["pagerequest_shortpath"][0]

        if request == "getpagelist":
            return str(PageManager().getPagesListAsJSON(request_shortpath)).encode()
