#!/usr/bin/condapython
"""
    Vera Listener:

    This class provides a method for receiving Vera events

Very simple HTTP procserver in python.
Usage::
    ./dummy-web-veraListener.py [<port>]
Send a GET request::
    curl http://localhost
Send a HEAD request::
    curl -I http://localhost
Send a POST request::
    curl -d "foo=bar&bin=baz" http://localhost
"""
from http.server import BaseHTTPRequestHandler, HTTPServer
from urllib.parse import urlparse, parse_qs

from src.util.singleton import Singleton
from src.config.params import Params
from src.config.logger import Logger
from src.util.MutexLock import MutexLock
from src.debter.eventsprocessor import EventsProcessor

import urllib.request
import threading
import binascii


class Server(BaseHTTPRequestHandler):
    '''
        Basic web procserver for listening to events
    '''

    def _set_headers(self,headerType="text/html"):
        self.send_response(200)
        self.send_header('Content-type', headerType)
        self.end_headers()
        print("procserver started ")

    def do_GET(self):
        url_message = parse_qs(urlparse(self.path).query)
        msg, msg_header = WebInterface().processGETEvent(url_message)

        self._set_headers(msg_header)
        self.wfile.write(msg)

    def do_HEAD(self):
        self._set_headers()
        print("head recieved ")

    def do_POST(self):
        url_message = parse_qs(urlparse(self.path).query)
        msg, msg_header = WebInterface().processPOSTEvent(url_message)

        self._set_headers(msg_header)
        self.wfile.write(msg)


class WebInterface(MutexLock, metaclass=Singleton):

    def __init__(self):
        '''
            Constructor for the listener
        :return:
        '''

        # call the mutex lock
        super(WebInterface, self).__init__()

        self.isServerStartedFlag = False

    def __del__(self):
        '''
            close up procserver
        :return:
        '''
        Logger().writeNote("WebInterface: destructor cleaning up")
        self.stop()

    def start(self, port=26000):
        '''
            Starts the procserver
        :param port:
        :return:
        '''

        self.threadLock()

        if not self.isServerStartedFlag:
            Logger().writeNote("WebInterface: Starting VeraListener Server on port=" + str(port))

            # copy the procserver port
            self.server_address = ('', port)  # ( open ip, port)

            try:
                # initialize the procserver
                self.httpd = HTTPServer(self.server_address, Server)

                Logger().writeNote("WebInterface: HTTPServer started ")
                # start the procserver in a seperate thread
                self.runThread = ServerThread(1, "HTTPServer Thread", self.httpd)

                self.runThread.start()

                self.isServerStartedFlag = True

                self.runproc = EventsProcessor()
            except OSError:

                Logger().writeNote("WebInterface:  Error- unable to start web procserver")
                Logger().dump()

        self.threadUnlock()

    def processGETEvent(self, message):
        '''
            This handles GET and POST messages to this procserver
        :return:
        '''


        self.threadLock()

        msg, msg_header = EventsProcessor().processGETEvent(message)

        self.threadUnlock()

        return msg,msg_header

    def processPOSTEvent(self, message):
        '''
            This handles GET and POST messages to this procserver
        :return:
        '''


        self.threadLock()

        msg,msg_header = EventsProcessor().processPOSTEvent(message)

        self.threadUnlock()

        return msg,msg_header

    def getDeviceValue(self, device_id, sensor_id, variable_id):
        '''
            Get device state information
        :param device_id: device id by label
        :param request_type: command, or get_state
        :param request: Request to be sent
        :return:
        '''

        self.threadLock()

        # deviceID = device_id  # 10
        # varID = variable_id  # 'CurrentTemperature'
        # serviceID = sensor_id  # 'TemperatureSensor1'
        # vera_addr = Params().get("vera_device_address")  # "http://192.168.0.101:3480"
        # request = vera_addr + "/data_request?id=lu_variableget&output_format=json&DeviceNum=" + str(
        #     deviceID) + "&serviceId=urn:upnp-org:serviceId:" + serviceID + "&Variable=" + varID
        #
        # f = urllib.request.urlopen(request)
        # variable_value = f.read()
        #
        # Logger().writeNote(" VeraDriver: getDeviceValue - got value from device=" + str(deviceID) + " serviceid=" + str(
        #     serviceID) + " value=" + str(variable_value))

        self.threadUnlock()

        return variable_value

    def setDeviceValue(self, device_id, sensor_id, device_value):
        '''
            Get device state information
        :param device_id: device id by label
        :param request_type: command, or get_state
        :param request: Request to be sent
        :return:
        '''

        self.threadLock()

        # deviceID = device_id  # 21
        # deviceValue = device_value  # 0
        # serviceID = sensor_id  # 'SwitchPower1'
        # veraaddr = Params().get("vera_device_address")  # "http://192.168.0.101:3480"
        # request = veraaddr + "/data_request?id=lu_action&output_format=json&DeviceNum=" + str(
        #     deviceID) + "&serviceId=urn:upnp-org:serviceId:" + serviceID + "&action=SetTarget&newTargetValue=" + str(
        #     deviceValue)
        #
        # f = urllib.request.urlopen(request)
        # print(f.read())
        #
        # Logger().writeNote(" VeraDriver: setDeviceValue - set value for device=" + str(deviceID) + " serviceid=" + str(
        #     serviceID) + " value=" + str(deviceValue))

        self.threadUnlock()

    def stop(self):
        '''
            Stops the procserver
        :return:
        '''

        Logger().writeNote("VeraDriver: stopping procserver- cleaning up")
        if self.isServerStartedFlag and self.runThread.is_alive():
            Logger().writeNote("VeraDriver: stoppingserver thread")
            self.runThread.serverObject._BaseServer__shutdown_request = True
            del self.runThread
            self.isServerStartedFlag = False


class ServerThread(threading.Thread):
    def __init__(self, threadID, name, serverObject):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
        self.serverObject = serverObject

        self.setName(name)

    def run(self):
        self.serverObject.serve_forever()
