
from src.util.singleton import Singleton

from src.config.params import Params

from src.config.logger import Logger

from src.util.MutexLock import MutexLock

import glob
import json

# This class is a singleton
class PageManager(MutexLock,metaclass=Singleton):
    '''
        This handles the pages directory
    '''

    def __init__(self):
        '''
            Initialize the class
        :return:
        '''

        # call the mutex lock
        super(PageManager, self).__init__()

        self.ext = Params().get("pages_extension")
        self.pagesDir = Params().get("pages_directory")
        self.pagesList = {}

    def __addFilenameToList(self,fullname):

        shortpath = fullname.replace(self.pagesDir, '')
        path = shortpath.split('/')

        dirPaths = path[1:-1]
        filename = path[-1].replace('.'+self.ext,'')

        dirpointer = self.pagesList
        for n in range(len(dirPaths)):
            if dirPaths[n] not in dirpointer:
                dirpointer[dirPaths[n]] = {}
            dirpointer = dirpointer[dirPaths[n]]

        if "__filename_by_fullname__" not in dirpointer:
            dirpointer["__filename_to_fullpath__"] = {}
            dirpointer["__shortpath_to_filename__"] = {}
            dirpointer["__fullpath_to_filename__"] = {}
            dirpointer["__files_list__"]=[]

        dirpointer["__filename_to_fullpath__"][filename] = fullname
        dirpointer["__shortpath_to_filename__"][shortpath] = filename
        dirpointer["__fullpath_to_filename__"][fullname] = filename
        dirpointer["__files_list__"].append(filename)

        for f in dirPaths:

            if f in self.pagesList:
                print(f)

    def __updatePagesList(self,rootPage=''):
        '''

        :param root:
        :return:
        '''
        self.pagesList = {}

        # this gets all the filenames
        for filename in glob.iglob(self.pagesDir+rootPage+'/**/*.'+self.ext, recursive=True):

            self.__addFilenameToList(filename)


    def getPagesListAsJSON(self,rootPage=''):

        if len(rootPage)<2:
            rootPage=''
        rootPage = ''

        self.__updatePagesList(rootPage)

        return json.dumps(self.pagesList,sort_keys=True, indent=4)

    def getPageFullPath(self, shortpath):
        '''

        :param path:
        :return:
        '''

        fileFound = False
        fullfilepath = ""

        self.pagesList = {}
        self.__updatePagesList()

        path = shortpath.split('/')

        dirPaths = path[1:-1]
        filename = path[-1].replace('.'+self.ext, '')

        dirpointer = self.pagesList

        for n in range(len(dirPaths)):
            if dirPaths[n] in dirpointer:
                dirpointer = dirpointer[dirPaths[n]]

        if "__filename_to_fullpath__" in dirpointer:
            fullfilepath = dirpointer["__filename_to_fullpath__"]
            fileFound = True

        return fileFound, fullfilepath


    def getPageData(self,shortpath):
        '''

        :param shortpath:
        :return:
        '''

    def writePageDta(self,shortpath,data):
        '''

        :param shortpath:
        :param data:
        :return:
        '''
    def createPage(self,shortpath):
        '''

        :param shortpath:
        :return:
        '''

    def deletePage(self,shortpath):
        '''

        :param shortpath:
        :return:
        '''

    def createDir(self,shortpath):
        '''

        :param shortpath:
        :return:
        '''

    def deleteDir(self,shortpath):
        '''

        :param shortpath:
        :return:
        '''