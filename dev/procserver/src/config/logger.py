'''

    Parameters

'''
import threading,sys
import time,datetime
from src.util.singleton import Singleton
from src.config.params import Params
#from src.controller import Controller

NOTIFY=0
WARN=1
ERROR=2

class Logger(metaclass=Singleton):

    is_running = False

    def __init__(self):
        '''
            Start storing parameters
        :return:
        '''
        self.lock = threading.Lock()
        self.log = []

        msg = (self.__gettime(),NOTIFY,"Logger Started")
        self.log.append(msg)

        self.params = Params()
        self.verbose = self.params.get("verbose")
        self.is_running = True

    def dump(self):
        filepath = self.params.get("log_output_path")
        fid = open(filepath,"w")
        type=["NOTE   ","WARNING","ERROR  "]

        for entry in self.log:
            fid.write(str(entry[0])+",   "+type[entry[1]]+": "+str(entry[2])+"\n")

        fid.close()

    def close(self):
        self.dump()

    def __gettime(self):
        return datetime.datetime.now().strftime("%I:%M%p %B-%d-%Y") # '10:36AM on July 23, 2010'

    def __print(self,msg):
        print("::: Logger: "+msg)

    def writeError(self,msg):

        self.lock.acquire()
        lg = (self.__gettime(),ERROR,msg)
        self.log.append(lg)
        if self.verbose:
            self.__print(msg)
        self.lock.release()

        lg = (self.__gettime(),ERROR," Logger received an error, Logger is shutting down program ")
        self.log.append(lg)

        sys.exit(0)

    def writeWarning(self,msg):
        self.lock.acquire()
        lg = (self.__gettime(),WARN,msg)
        self.log.append(lg)
        if self.verbose:
            self.__print(msg)
        self.lock.release()

    def writeNote(self,msg):
        self.lock.acquire()
        lg = (self.__gettime(),NOTIFY,msg)
        self.log.append(lg)
        if self.verbose:
            self.__print(msg)
        self.lock.release()