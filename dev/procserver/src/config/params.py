'''

    Parameters

'''
import threading
from src.util.singleton import Singleton
from src.util.errorhandler import ErrorHandler
import datetime
import time
import json
from pprint import pprint

class Params(metaclass=Singleton):

    def __init__(self):
        '''
            Start storing parameters
        :return:
        '''
        self.lock = threading.Lock()
        self.db = {}

    def loadDataFromConfigFile(self, configFilePath):
        '''

        :param configFilePath:
        :return:
        '''
        with open(configFilePath) as data_file:
            self.db = json.load(data_file)

        date = datetime.date.today().strftime("%B-%d-%Y-%h")
        self.set("log_output_path",self.get("log_output_directory")+"/log_"+date+".log")

    def set(self,param_label,param_value):
        '''
            Set a parameter value
        :param param_label:
        :param param_value:
        :return:
        '''

        self.lock.acquire()

        self.db[param_label] = param_value

        self.lock.release()

        return True

    def get(self,param_label):
        '''
            get the value of a parameter
        :param param_label:
        :return:
        '''

        self.lock.acquire()
        if param_label in self.db:
            val = self.db[param_label]

        else:
            ErrorHandler().writeError("Param() unable to find parameter :"+param_label)
        self.lock.release()

        return val