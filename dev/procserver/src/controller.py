'''

    This is the main controller and runs the show

'''

from src.util.singleton import Singleton

from src.config.logger import Logger
from src.config.params import Params
from src.debter.eventsprocessor import EventsProcessor
from src.debter.webserver import WebInterface


class Controller(metaclass=Singleton):

    def __init__(self, config_filepath, use_gui_flag, use_web_flag, web_server_port, verbose_flag):
        '''
            This initializes the whole routine
        :return:
        '''
        # call the mutex lock
        super(Controller,self).__init__()

        # set variables
        p = Params()
        # copy parameters from file
        p.loadDataFromConfigFile(config_filepath)

        p.set("use_gui_flag",use_gui_flag)
        p.set("use_web_server_flag",use_web_flag)
        p.set("web_server_port",int(web_server_port))
        p.set("verbose",verbose_flag)

        # start the logger
        log=Logger()

        log.writeNote("Starting Controller")

        # initialize the drivers

    def run(self):
        '''
            This runs
        :return:
        '''

        Logger().writeNote("Controller: Starting runner")

        # start up the event processor
        self.event = EventsProcessor()

        # start the web server
        self.web = WebInterface()
        self.web.start(port=Params().get("web_server_port"))


    def close(self):
        '''
            Close the drive
        :return:
        '''
        log = Logger()
        log.writeNote("Controller: shutdown process triggered")

        # shutdown the drivers
        self.web.stop()

        log.close()
