'''

    This provides an easy way for locking functions

'''
import threading,sys
import time,datetime

class MutexLock:

    def __init__(self):
        '''
            Define some basic locking
        :return:
        '''
        self.lock = threading.Lock()

    def threadLock(self):
        self.lock.acquire()

    def threadUnlock(self):
        self.lock.release()