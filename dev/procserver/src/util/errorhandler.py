
from src.util.singleton import Singleton
import pprint


class ErrorHandler(metaclass=Singleton):

    def __init__(self):
        '''
            INIT
        :return:
        '''

    def writeError(self,msg):
        pprint(msg)
        exit()