import unittest

from src.drivers.telephone.plivoDriver import PlivoDriver

from src.config.params import Params

class TestPlivo(unittest.TestCase):

    def setUp(self):
        self.params = Params()

    def test_call(self):
        configPath="/Users/btesfaye/Birukus/dev/pyGojo/test/data/config/gojo_config.json"

        self.params.loadDataFromConfigFile(configPath)
        p = PlivoDriver()
        p.call2()
        self.assertEqual('foo'.upper(), 'FOO')

