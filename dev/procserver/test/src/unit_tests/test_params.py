import unittest

from src.config.params import Params

class TestParams(unittest.TestCase):

    def setUp(self):
        self.params = Params()

    def test_configRead(self):
        configPath="/Users/btesfaye/Birukus/dev/pyGojo/test/data/config/gojo_config.json"

        self.params.loadDataFromConfigFile(configPath)
        self.assertEqual(self.params.get("header"), "GOJO Config file")

    def test_setter(self):

        self.assertTrue(self.params.set("log_output_path", "test_path"))
        self.assertEqual(self.params.get("log_output_path"), "test_path")

    def test_getter(self):

        self.assertFalse(self.params.get("use_gui_flag"))

    def test_getZwaveDeviceByGroupName(self):
        grp = self.params.getZwaveDeviceByGroupName("ThermostatGroup")
        self.assertEqual(len(grp),2,"Group size test")
        #with self.assertRaises(SystemExit):
        #    self.params.get("use_gui_flag2")
    def test_getZwaveDeviceByLabel(self):
        dev = self.params.getZwaveDeviceByLabel("LightSwitch1")
        self.assertEqual(dev.sensorID, "SwitchPower1", " sensor id read test")
