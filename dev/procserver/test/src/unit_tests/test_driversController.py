import unittest

from src.controller import Params
from src.controller import Controller
from src.drivers.driverscontroller import DriversController

class TestDriversController(unittest.TestCase):

    @classmethod
    def setUpClass(self):
        """ get_some_resource() is slow, to avoid calling it for each test use setUpClass()
            and store the result as class variable
        """
        super(TestDriversController, self).setUpClass()

        print("***** setup class ********")
        configPath="/Users/btesfaye/Birukus/dev/pyGojo/test/data/config/gojo_config.json"

        self.control = Controller(configPath,False,False,28008,True)

        self.control.run()

        self.driver = DriversController()

    @classmethod
    def tearDownClass(self):
        print("***** delete class ********")
        self.control.close()

    def test_startDrivers(self):
        self.assertTrue(self.driver.isControllerRunning,"is Controller running test")

    def test_sendDeviceCommand(self):
        '''
            Send device data
        :return:
        '''

        # vera test
        self.driver.sendDeviceCommand(Params().get("zwave_device_type"),"0","get_value","temperature")

    def test_receiveEventMessage(self):
        '''

        :return:
        '''
        # addr="http://192.168.0.104:3480"
        # msg="data_request?id=lu_action&output_format=json&DeviceNum=21&serviceId=urn:upnp-org:serviceId:SwitchPower1&action=SetTarget&newTargetValue=1"
        # curl --data $msg $addr
        # echo curl --data $msg $addr


    def tearDown(self):

        self.control.close()