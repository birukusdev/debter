import unittest

from src.config.logger import Logger

class TestLogger(unittest.TestCase):

    def setUp(self):
        self.logger = Logger()

    def test_configRead(self):
        configPath="/Users/btesfaye/Birukus/dev/pyGojo/test/data/config/gojo_config.json"

        self.params.loadDataFromConfigFile(configPath)
        self.assertEqual(self.params.get("header"), "GOJO Config file")

    def test_setter(self):

        self.assertTrue(self.params.set("log_output_path", "test_path"))
        self.assertEqual(self.params.get("log_output_path"), "test_path")

    def test_getter(self):

        self.assertFalse(self.params.get("use_gui_flag"))

        #with self.assertRaises(SystemExit):